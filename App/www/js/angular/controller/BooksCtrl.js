angular.module('App').controller('BooksCtrl', [
    '$scope', '$http', '$location', function($scope, $http, $location) {

    books = $http({
            url : '/api.php', 
            method: 'GET',
        }).success(function(data){
            //console.log(data.search.results.work[0].best_book.title);
            return $scope.books = data.search.results.work;
        }).error(function(data) {
            console.log('erreur de connexion à l\'API');
        });
    }
]);