angular.module('App').controller('MoviesCtrl', [
    '$scope', '$http', '$location', function($scope, $http, $location) {

        $http({
            url : 'http://api.themoviedb.org/3/keyword/10028/movies?api_key=c39c634a043801182da70d7b7c61636f', 
            method: 'GET',
        }).success(function(data){
            return $scope.films = data.results;
        }).error(function(data) {
            console.log('erreur de connexion à l\'API');
        });

        var video = document.getElementsByTagName("video")[0];
        video.removeAttribute('controls');

        var controls = document.getElementById("controls");
        $scope.play = false;
        $scope.pause = true;

        $scope.toggleVideo = function(){
            if($scope.pause){
                //if (video.ended) vieo.currentTime = 0;
                video.play();
                $scope.play = true;
                $scope.pause = false;
            }
            else if($scope.play){
                video.pause();
                $scope.pause = true;
                $scope.play = false;
            }
        }
        
    }
    
]);