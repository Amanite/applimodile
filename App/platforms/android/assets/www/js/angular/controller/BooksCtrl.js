angular.module('App').controller('BooksCtrl', [
    '$scope', '$http', '$location', function($scope, $http, $location) {

    data = {
               "Request": {
                   "authentication": "true",
                   "key": {},
                   "method": {}
               },
               "search": {
                   "query": {},
                   "results-start": "1",
                   "results-end": "20",
                   "total-results": "3483",
                   "source": "Goodreads",
                   "query-time-seconds": "0.13",
                   "results": {
                       "work": [
                           {
                               "id": "2251965",
                               "books_count": "3",
                               "ratings_count": "1654",
                               "text_reviews_count": "181",
                               "original_publication_year": "2008",
                               "original_publication_month": "5",
                               "original_publication_day": "1",
                               "average_rating": "3.51",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "2246092",
                                   "title": "Steampunk (Steampunk, #1)",
                                   "author": {
                                       "id": "33919",
                                       "name": "Jeff VanderMeer"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1395146110m\/2246092.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1395146110s\/2246092.jpg"
                               }
                           },
                           {
                               "id": "4836639",
                               "books_count": "1493",
                               "ratings_count": "673909",
                               "text_reviews_count": "13299",
                               "original_publication_year": "1818",
                               "original_publication_month": {
                                   "@attributes": {
                                       "type": "integer",
                                       "nil": "true"
                                   }
                               },
                               "original_publication_day": {
                                   "@attributes": {
                                       "type": "integer",
                                       "nil": "true"
                                   }
                               },
                               "average_rating": "3.71",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "18490",
                                   "title": "Frankenstein",
                                   "author": {
                                       "id": "11139",
                                       "name": "Mary Shelley"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1381512375m\/18490.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1381512375s\/18490.jpg"
                               }
                           },
                           {
                               "id": "11080703",
                               "books_count": "1",
                               "ratings_count": "15",
                               "text_reviews_count": "4",
                               "original_publication_year": "2010",
                               "original_publication_month": "1",
                               "original_publication_day": "1",
                               "average_rating": "4.33",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "7880873",
                                   "title": "Steampunk",
                                   "author": {
                                       "id": "5586907",
                                       "name": "\u00c9tienne Barillier"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1329983027m\/7880873.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1329983027s\/7880873.jpg"
                               }
                           },
                           {
                               "id": "15242493",
                               "books_count": "6",
                               "ratings_count": "654",
                               "text_reviews_count": "53",
                               "original_publication_year": "2011",
                               "original_publication_month": "10",
                               "original_publication_day": "4",
                               "average_rating": "4.10",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "10339809",
                                   "title": "Steampunk: Poe",
                                   "author": {
                                       "id": "4624490",
                                       "name": "Edgar Allan Poe"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1344668167m\/10339809.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1344668167s\/10339809.jpg"
                               }
                           },
                           {
                               "id": "12925396",
                               "books_count": "6",
                               "ratings_count": "500",
                               "text_reviews_count": "53",
                               "original_publication_year": "2010",
                               "original_publication_month": "1",
                               "original_publication_day": "1",
                               "average_rating": "3.69",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "8129662",
                                   "title": "Steampunk II: Steampunk Reloaded (Steampunk, #2)",
                                   "author": {
                                       "id": "410598",
                                       "name": "Ann VanderMeer"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1289014290m\/8129662.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1289014290s\/8129662.jpg"
                               }
                           },
                           {
                               "id": "15685538",
                               "books_count": "2",
                               "ratings_count": "21",
                               "text_reviews_count": "1",
                               "original_publication_year": "2011",
                               "original_publication_month": "1",
                               "original_publication_day": "20",
                               "average_rating": "4.19",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "10773798",
                                   "title": "Steampunk",
                                   "author": {
                                       "id": "6538146",
                                       "name": "Instructables"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1328046363m\/10773798.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1328046363s\/10773798.jpg"
                               }
                           },
                           {
                               "id": "14046028",
                               "books_count": "25",
                               "ratings_count": "14983",
                               "text_reviews_count": "2076",
                               "original_publication_year": "2011",
                               "original_publication_month": "1",
                               "original_publication_day": "1",
                               "average_rating": "3.84",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "9166877",
                                   "title": "The Girl in the Steel Corset (Steampunk Chronicles, #1)",
                                   "author": {
                                       "id": "4308706",
                                       "name": "Kady Cross"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1297987541m\/9166877.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1297987541s\/9166877.jpg"
                               }
                           },
                           {
                               "id": "6676256",
                               "books_count": "8",
                               "ratings_count": "2311",
                               "text_reviews_count": "301",
                               "original_publication_year": "2010",
                               "original_publication_month": {
                                   "@attributes": {
                                       "type": "integer",
                                       "nil": "true"
                                   }
                               },
                               "original_publication_day": {
                                   "@attributes": {
                                       "type": "integer",
                                       "nil": "true"
                                   }
                               },
                               "average_rating": "2.96",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "6484914",
                                   "title": "Steamed: a steampunk romance",
                                   "author": {
                                       "id": "28550",
                                       "name": "Katie MacAlister"
                                   },
                                   "image_url": "http:\/\/s.gr-assets.com\/assets\/nophoto\/book\/111x148-bcc042a9c91a29c1d680899eff700a03.png",
                                   "small_image_url": "http:\/\/s.gr-assets.com\/assets\/nophoto\/book\/50x75-a91bf249278a81aabab721ef782c4a74.png"
                               }
                           },
                           {
                               "id": "14158747",
                               "books_count": "4",
                               "ratings_count": "1521",
                               "text_reviews_count": "174",
                               "original_publication_year": "2011",
                               "original_publication_month": "5",
                               "original_publication_day": "1",
                               "average_rating": "3.95",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "9276833",
                                   "title": "The Steampunk Bible",
                                   "author": {
                                       "id": "33919",
                                       "name": "Jeff VanderMeer"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1325809765m\/9276833.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1325809765s\/9276833.jpg"
                               }
                           },
                           {
                               "id": "15242487",
                               "books_count": "1",
                               "ratings_count": "6",
                               "text_reviews_count": "0",
                               "original_publication_year": "2011",
                               "original_publication_month": "1",
                               "original_publication_day": "21",
                               "average_rating": "4.67",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "10339803",
                                   "title": "Steampunk",
                                   "author": {
                                       "id": "4612225",
                                       "name": "Nathan Hawk"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1327953980m\/10339803.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1327953980s\/10339803.jpg"
                               }
                           },
                           {
                               "id": "19058959",
                               "books_count": "9",
                               "ratings_count": "4793",
                               "text_reviews_count": "705",
                               "original_publication_year": "2012",
                               "original_publication_month": "9",
                               "original_publication_day": "1",
                               "average_rating": "3.93",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "13507512",
                                   "title": "Kiss of Steel (London Steampunk, #1)",
                                   "author": {
                                       "id": "5763301",
                                       "name": "Bec McMaster"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1344055795m\/13507512.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1344055795s\/13507512.jpg"
                               }
                           },
                           {
                               "id": "16936400",
                               "books_count": "16",
                               "ratings_count": "6766",
                               "text_reviews_count": "818",
                               "original_publication_year": "2012",
                               "original_publication_month": "5",
                               "original_publication_day": "22",
                               "average_rating": "4.02",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "13060608",
                                   "title": "The Girl in the Clockwork Collar (Steampunk Chronicles, #2)",
                                   "author": {
                                       "id": "4308706",
                                       "name": "Kady Cross"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1361759116m\/13060608.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1361759116s\/13060608.jpg"
                               }
                           },
                           {
                               "id": "16053889",
                               "books_count": "8",
                               "ratings_count": "5750",
                               "text_reviews_count": "793",
                               "original_publication_year": "2011",
                               "original_publication_month": "5",
                               "original_publication_day": "1",
                               "average_rating": "3.77",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "11130686",
                                   "title": "The Strange Case of Finley Jayne (Steampunk Chronicles, #0.5)",
                                   "author": {
                                       "id": "4308706",
                                       "name": "Kady Cross"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1303414416m\/11130686.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1303414416s\/11130686.jpg"
                               }
                           },
                           {
                               "id": "23628799",
                               "books_count": "2",
                               "ratings_count": "1238",
                               "text_reviews_count": "149",
                               "original_publication_year": "2013",
                               "original_publication_month": "4",
                               "original_publication_day": "5",
                               "average_rating": "3.81",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "17181953",
                                   "title": "Tarnished Knight (London Steampunk, #1.5)",
                                   "author": {
                                       "id": "5763301",
                                       "name": "Bec McMaster"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1363144626m\/17181953.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1363144626s\/17181953.jpg"
                               }
                           },
                           {
                               "id": "1160689",
                               "books_count": "13",
                               "ratings_count": "636",
                               "text_reviews_count": "78",
                               "original_publication_year": "1995",
                               "original_publication_month": {
                                   "@attributes": {
                                       "type": "integer",
                                       "nil": "true"
                                   }
                               },
                               "original_publication_day": {
                                   "@attributes": {
                                       "type": "integer",
                                       "nil": "true"
                                   }
                               },
                               "average_rating": "3.48",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "326917",
                                   "title": "The Steampunk Trilogy",
                                   "author": {
                                       "id": "162927",
                                       "name": "Paul Di Filippo"
                                   },
                                   "image_url": "http:\/\/s.gr-assets.com\/assets\/nophoto\/book\/111x148-bcc042a9c91a29c1d680899eff700a03.png",
                                   "small_image_url": "http:\/\/s.gr-assets.com\/assets\/nophoto\/book\/50x75-a91bf249278a81aabab721ef782c4a74.png"
                               }
                           },
                           {
                               "id": "21531015",
                               "books_count": "5",
                               "ratings_count": "69",
                               "text_reviews_count": "15",
                               "original_publication_year": "2012",
                               "original_publication_month": {
                                   "@attributes": {
                                       "type": "integer",
                                       "nil": "true"
                                   }
                               },
                               "original_publication_day": {
                                   "@attributes": {
                                       "type": "integer",
                                       "nil": "true"
                                   }
                               },
                               "average_rating": "3.55",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "15806684",
                                   "title": "Steampunk III: Steampunk Revolution",
                                   "author": {
                                       "id": "410598",
                                       "name": "Ann VanderMeer"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1395146032m\/15806684.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1395146032s\/15806684.jpg"
                               }
                           },
                           {
                               "id": "21678148",
                               "books_count": "8",
                               "ratings_count": "2208",
                               "text_reviews_count": "286",
                               "original_publication_year": "2013",
                               "original_publication_month": "5",
                               "original_publication_day": "7",
                               "average_rating": "4.03",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "15927484",
                                   "title": "Heart of Iron (London Steampunk, #2)",
                                   "author": {
                                       "id": "5763301",
                                       "name": "Bec McMaster"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1366437375m\/15927484.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1366437375s\/15927484.jpg"
                               }
                           },
                           {
                               "id": "21552214",
                               "books_count": "11",
                               "ratings_count": "3790",
                               "text_reviews_count": "469",
                               "original_publication_year": "2013",
                               "original_publication_month": "5",
                               "original_publication_day": "17",
                               "average_rating": "3.97",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "15822336",
                                   "title": "The Girl with the Iron Touch (The Steampunk Chronicles, #3)",
                                   "author": {
                                       "id": "4308706",
                                       "name": "Kady Cross"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1354562654m\/15822336.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1354562654s\/15822336.jpg"
                               }
                           },
                           {
                               "id": "23970725",
                               "books_count": "3",
                               "ratings_count": "1389",
                               "text_reviews_count": "191",
                               "original_publication_year": "2013",
                               "original_publication_month": "10",
                               "original_publication_day": "1",
                               "average_rating": "4.15",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "17310126",
                                   "title": "My Lady Quicksilver (London Steampunk, #3)",
                                   "author": {
                                       "id": "5763301",
                                       "name": "Bec McMaster"
                                   },
                                   "image_url": "http:\/\/d.gr-assets.com\/books\/1380024092m\/17310126.jpg",
                                   "small_image_url": "http:\/\/d.gr-assets.com\/books\/1380024092s\/17310126.jpg"
                               }
                           },
                           {
                               "id": "15544018",
                               "books_count": "14",
                               "ratings_count": "2620",
                               "text_reviews_count": "466",
                               "original_publication_year": "2011",
                               "original_publication_month": "10",
                               "original_publication_day": "11",
                               "average_rating": "3.68",
                               "best_book": {
                                   "@attributes": {
                                       "type": "Book"
                                   },
                                   "id": "10635363",
                                   "title": "Steampunk! An Anthology of Fantastically Rich and Strange Stories",
                                   "author": {
                                       "id": "24902",
                                       "name": "Kelly Link"
                                   },
                                   "image_url": "http:\/\/s.gr-assets.com\/assets\/nophoto\/book\/111x148-bcc042a9c91a29c1d680899eff700a03.png",
                                   "small_image_url": "http:\/\/s.gr-assets.com\/assets\/nophoto\/book\/50x75-a91bf249278a81aabab721ef782c4a74.png"
                               }
                           }
                       ]
                   }
               }
           };
        $scope.books = data.search.results.work;
    }
]);